const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TrackHistorySchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    red: 'User',
    required: true,
  },
  track: {
    type: Schema.Types.ObjectId,
    red: 'Track',
    required: true,
  },
  datetime: {
    type: String,
    required: true,
  }
});

const TrackHistory = mongoose.model('TrackHistory', TrackHistorySchema);

module.exports = TrackHistory;