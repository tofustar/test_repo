export class Album {
  constructor(
    public _id: string,
    public name: string,
    public artist: string,
    public year: string,
    public image: string,
  ){}
}

export interface ApiAlbumData {
  _id: string,
  name: string,
  artist: string,
  year: string,
  image: string
}