export interface TrackHistory {
  _id: string,
  user: string,
  track: string,
  datetime: string,
}